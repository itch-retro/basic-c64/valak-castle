10 gosub 9000
12 poke 646,1:poke 53280,0:poke 53281,0
13 goto 21

15 rem draw subroutines
16 print "  ====================================  "
17 return
18 print "  ";chr$(116);"                                  ";chr$(121)
19 return

20 rem splash screen
21 print chr$(147)
22 gosub 16:gosub 18
23 print "  ";chr$(116);"          valak castle            ";chr$(121)
24 gosub 18:gosub 16
25 print:input "   1-iniciar 2-creditos 3-sair";o
26 if o=50 then gosub 50
27 if o<>1 and o<>2 and o<>3 then goto 20
28 on o goto 70,40,30

29 rem end subroutine
30 print chr$(147)
31 poke 646,14:poke 53280,14:poke 53281,6
32 end

40 rem credits subroutine
41 print chr$(147):gosub 16:gosub 18
42 print "  ";chr$(116);"    design, roteiro, programacao  ";chr$(121)
43 print "  ";chr$(116);"         por lucas goncalves      ";chr$(121)
44 gosub 18:gosub 16
45 print:input "   1-voltar ao menu";o
46 if o<>1 then goto 40
47 on o goto 20

50 rem rolagem de d20
51 print chr$(147)
52 print "rolar d20 para ";y$;", cd ";c:input t
53 let x=0
54 rem por 7 vezes
55 for i=1 to 7
56 print chr$(147)
57 rem randomizar / imprimir
58 let x=int(rnd(1)*20)+1
59 print x
60 rem aguardar 0.5 segundos
61 for j=1 to 200:next j
62 next i
63 print chr$(147)
64 print "voce tirou ";x
65 t=x
66 return

69 rem em frente ao castelo
70 print chr$(147)
71 print "voce esta na frente do castelo  do lordevalak, alem da entrada comum"
72 print "voce nota uma  entrada  atraves  de  umaparede destruida do castelo"
73 print "escondida entre as rochas. aparentementea passagem da nas masmorras"
74 print "abaixo do castelo."
75 print:print "1-entrar pelo portao frontal":print "2-entrar pela parede destruida"
76 print "3-dar meia volta":input o
77 if o=101010 then goto 30
78 if o<>1 and o<>2 and o<>3 then goto 70
79 on o goto 80,100,120

80 y$="furtividade":c=15:print chr$(147): rem portao frontal do castelo
81 print "os guardas estao atentos  para  qualquerintruso, voce tenta entrar pelo"
82 print "portao frontal sem ser notado":input o:gosub 50
83 if t=20 then goto 89
84 if t>=c then goto 90
85 if t=1 then goto 87
86 if t<c then goto 88
87 print "seus passos sao como o  estrondo  de  umviolento trovao"
88 print "os guardas imediatamente  notam  voce  ete matam... fim de jogo":input o:goto 40
89 print "voce e uma sombra, ninguem  ve  ou  ouvevoce"
90 print "voce consegue entrar sem ser notado.":input o:goto 130

100 y$="resistencia":c=8:print chr$(147): rem entrada pela masmorra
101 print "chegando perto voce nota  que  a  paredequebrada da acesso atraves do teto"
102 print "da masmorra  no  subsolo.  devera  pularpara cair na masmorra":input o:gosub 50
103 if t=20 then goto 111
104 if t>=c then goto 112
105 if t=1 then goto 107
106 if t<c then goto 109
107 print "de maneira desengonsada  voce  escorregaao pular e cai de cabeça"
108 print "morrendo instataneamente... fim de jogo":input o:goto 40
109 print "de maneira  desengonsada voce  escorregaao pular e cai de peito"
110 print "no chao, esta sentindo muita dor agora":input o:goto 150
111 print "voce pula  e  realiza  uma  aterrissagematletica e perfeita"
112 print "voce esta  no  primeiro  nivel  das mas-morras":input o:goto 150

120 rem desistencia da aventura
121 print "voce sente um  frio  estranho,  um  medodescomunal percorre sua coluna."
126 print "amedrontado  voce da meia volta  e  fogeda aventura...":input o:goto 40

130 rem hall do castelo
131 print chr$(147)
132 print "o hall de entrada do castelo e uma  obra-prima sombriamente majestosa."
133 print "paredes de pedra macica,  vitrais  colo-ridos e uma imponente escadaria"
134 print "a sua frente. uma  porta  ornamentada  aesquerda da escadaria, outra"
135 print "grandiosa   porta  ornamentada  a  lesteindicam caminhos intrigantes."
136 print "a oeste, um arco de pedra com ornamentosde pessoas acorrentadas revela"
137 print "uma  escadaria   espiral  iluminada  portochas, sugerindo segredos"
138 print "ocultos. o  ambiente, impregnado de mis-terio, e iluminado pelo"
139 print "crepitar das  tochas e sussurra histori-as antigas, proporcionando uma"
140 print "entrada  imponente no coracao do enigma-tico castelo."
141 print:print "1-oeste  2-porta ornamentada"
142 input "3-escadaria a frente  4-leste";o
143 if o=101010 then goto 30
144 if o<>1 and o<>2 and o<>3 and o<>4 then goto 130
145 on o goto 260,290,320,350

150 print chr$(147):rem primeiro nivel das masmorras
151 print "um local escuro e  estranho,  claramenteusado para causar muito"
152 print "sofrimento,  destruir  a  mente de  quemaqui foi aprisionado..."
153 print "voce percebe um corredor escuro ao oestecom um cheiro muito forte"
154 print "de morte. a leste um  corredor relativa-mente iluminado por tochas"
155 print "a sua frente nota um corredor  de  celasvazias com diversos pertences"
156 print "antigos de prisioneiros que ali  estive-ram."
157 print:input "1-oeste 2-celas 3-leste";o
158 if o=101010 then goto 30
159 if o<>1 and o<>2 and o<>3 then goto 150
160 on o goto 170,200,230

169 rem decisao - poco de corpos
170 if ac(1)=1 then goto 189
171 print chr$(147):y$="sanidade":c=9
172 print "ao caminhar pelo  corredor  escuro  vocecomeca a perceber que as"
173 print "paredes estao  umidas  de  sangue,  vocecaminha sobre restos mortais,"
174 print "viceras. conforme avanca o mal cheiro seintensifica mais e mais,"
175 print "voce nunca percorreu um caminho tao per-turbador quanto este...":input o
176 gosub 50
177 if t=20 then goto 186
178 if t>=c then goto 188
179 if t=1 then goto 181
180 if t<c then goto 184
181 print "voce entra em colapso, sentindo uma ago-nia esgrucitante voce infarta"
182 print "e sua alma se une aos incontaveis lamen-tos do castelo... fim de jogo"
183 input o:goto 40
184 print "sua mente se perde, voce ja nao controlamais seu sentidos, sera"
185 print "para sempre um ser vagante neste castelo... fim de jogo":input o:goto 40
186 print "sua determinação e inabalavel, todo esteterror te motiva mais"
187 print "a derrotar o demonio deste castelo."
188 print "voce prossegue determinado e firme":input o: goto 470
189 print "o caminho foi bloqueado pelas  paredes eo teto colapsado...":input o: goto 150

200 rem decisao - celas das masmorras
201 if ac(2)=1 then goto 227
202 print chr$(147):y$="percepcao":c=17
203 print "deseja vasculhar os pertences das celas?"
204 input "1-sim  2-nao";o
205 if o=101010 then goto 30
206 if o<>1 and o<>2 then goto 200
207 if o=2 then c=10
208 on o goto 209,210
209 print "voce busca algo, mas nada util  encontra... porem,":input o
210 print "durante sua exploracao  acaba  acionandouma armadilha mortal,"
211 print "espinhos longos e afiados surgem do chaoe das paredes..."
212 input o:gosub 50
213 if t=20 then goto 224
214 if t>=c then goto 226
215 if t=1 then goto 217
216 if t<c then goto 219
217 print "sem tempo para entender o que aconteceu,voce e instantaneamente"
218 print "empalado, sua aventura termina aqui...  fim de jogo":input o:goto 40
219 print "voce tenta desviar a  tempo,  porem  suacoxa direita e seu ombro esquerdo"
220 print "sao penetrados por  espinhos  metalicos,voce consegue remove-los"
221 print "e continua avancando alguns  metros, en-tretanto com o sangramento constante"
222 print "acaba caindo inconciente,  sua  alma in-conformada ira para sempre vagar"
223 print "neste castelo... fim de jogo":input o:goto 40
224 print "com  uma  reacao  descomunalmente  agil,voce desvia da armadilha com"
225 print "extrema facilidade, sua  determinacao tetorna mais agil":input o:goto 520
226 print "por  pouco,  voce  desvia  da  armadilhaileso...":input o:goto 520
227 print chr$(147)
228 print "voce passa cuidadosamente, desviando das laminas previamente ativadas"
229 input o:goto 520

230 rem acesso masmorra->hall goto 130
231 print chr$(147):print "voce prossegue pelo corredor, que termi-na em uma"
232 print "escada de pedra em espiral, subindo paraalgum lugar com mais"
233 print "luminosidade, voce sobe a  extensa esca-daria...":input o:goto 130

260 rem acesso hall->masmorra goto 150
261 print chr$(147):print "voce desce  a  grandiosa escadaria espi-ral por minutos"
262 print "quanto mais avanca na  descida, mais es-curo o caminho se mostra, ate"
263 print "que a escada termina em um corredor  maliluminado por tochas,"
264 print "sendo o  unico  caminho  possivel,  vocecomeca a avancar por ele..."
265 input o:goto 150

290 rem biblioteca
291 print chr$(147):print"voce esta  na  biblioteca uma camara im-ponente em dois andares,"
292 print "repleta de estantes de  madeira  escura,onde volumes empoeirados aguardam "
293 print "descoberta. a luz filtrada  por  vitraisgoticos pinta padroes sombrios"
294 print "no chao de pedra polida. mesas de carva-lho, cobertas por pergaminhos,"
295 print "ocupam o centro, sob o crepitar suave develas negras. uma escada"
296 print "espiral leva ao segundo  andar, onde es-treitos corredores entre as "
297 print "estantes criam um labirinto  de conheci-mento sombrio. cada passo"
298 print "nesse reino de segredos  antigos  revelauma atmosfera misteriosa,"
299 print "como se os proprios livros  sussurrassemsuas historias aos exploradores"
300 print "corajosos..."
301 print:print "1-ir para segundo andar":print "2-voltar para o hall de entrada"
302 input "3-seguir explorando a biblioteca"; o
303 if o=101010 then goto 30
304 if o<>1 and o<>2 and o<>3 then goto 290
305 on o goto 320,130,650

320 rem escadaria para segundo piso
321 print chr$(147):if ac(4)=1 then goto 329
322 print "uma estranha escadaria, ao olhar para cima, nao e possivel ver o segundo"
323 print "piso muito bem, parece obscurecido por algum tipo de nevoa escura..."
324 print "voce comeca a subir a escada, um degrau por vez. mas quanto mais voce"
325 print "sobe, mais distante o segundo piso parece... apos minutos de subida"
326 print "voce olha para tras e percebe que esta no primeiro degrau do primeiro"
327 print "piso da biblioteca. so entao percebe que algum tipo de magia obscura"
328 print "proibe sua subida ao segundo piso, por enquanto...":input o:goto 290
329 print "alguns poucos degraus de subida, e voce chega ao segundo piso da"
330 print "biblioteca...":input o:goto 550

350 rem salao comunal
351 print chr$(147)
352 print "o grande salao do castelo. arcos goticoscurvados como garras"
353 print "sombrias,  pairam  sobre  mesas  longas,cobertas por toalhas encharcadas"
354 print "de uma escuridao  profunda.  candelabrostremeluzem, revelando detalhes"
355 print "da sombria arquitetura  nas  paredes  depedra. tronos elevados,"
356 print "adornados com entidades  grotescas, ema-nam uma presença ameaçadora."
357 print "pinturas  murmuram  historias  sombrias,enquanto uma nevoa eterea envolve"
358 print "tudo.  gargulas,  esculpidas  com  facesdistorcidas, observam com olhos"
359 print "famintos."
360 print "quase escondido atras de uma  coluna  aofundo do grande salao, uma unica"
361 print "porta simples de  madeira,  sem  detalhealgum, destoando da arquitetura"
362 print "do local."
363 print "1-voltar para o hall"
364 input "2-porta ao fundo do salao";o
365 if o=101010 then goto 30
366 if o<>1 and o<>2 then goto 350
367 on o goto 130,580








370 rem rolagem de batalha ======================
371 print chr$(147):print "em combate":let j=1
372 let k=0:let l=0:if f=1 then goto 374
373 print "o inimigo: ";e$;" ataca primeiro":input o:goto 375
374 print "voce ataca primeiro":input o:goto 392 

375 for q=1 to 10: rem inimigo ataca primeiro
376 gosub 409:gosub 441
377 if el<=0 then goto 445
378 if jl<=0 then goto 448
379 input "1-atacar  2-fugir";o
380 if o<>1 and o<>2 then goto 379
381 on o goto 387, 382
382 gosub 50
383 if t>=c then print "voce fugiu de ";e$:return
384 print "ao falhar em fugir, voce recebe um ataque":input o:gosub 451
385 jl=jl-rl
386 goto 390
387 gosub 425:gosub 441
388 if el<=0 then goto 445
389 if jl<=0 then goto 448
390 next q
391 return

392 for q=1 to 10: rem jogador ataca primeiro
393 input "1-atacar  2-fugir";o
394 if o<>1 and o<>2 then goto 393
395 on o goto 401,396
396 gosub 50
397 if t>=c then print "voce fugiu de ";e$:return
398 print "ao falhar em fugir voce recebe um ataque":input o:gosub 451
399 jl=jl-rl
400 goto 404
401 gosub 425:gosub 441
402 if el<=0 then goto 445
403 if jl<=0 then goto 448
404 gosub 409:gosub 441
405 if el<=0 then goto 445
406 if jl<=0 then goto 448
407 next q
408 return


409 let x=0:let y=0:rem rolagem inimigo
410 for i=1 to 7
411 print chr$(147)
412 let x=int(rnd(1)*20)+1
413 print e$;" rolou ataque: ";x
414 let y=int(rnd(1)*20)+1
415 print "voce rolou defesa: "y
416 for j=1 to 200:next j
417 next i
418 if x>y then goto 421
419 if y>x then goto 424
420 if x=y then print "o inimigo erra"

421 l=l+1
422 print e$;" deferiu um ataque em voce":input o:y$="dano"
423 gosub 451:jl=(jl-rl):return

424 print "voce defendeu o ataque de ";e$:input o:return


 
425 let x=0:let y=0:rem rolagem jogador
426 for i=1 to 7:rem por 7 vezes
427 print chr$(147)
428 let x=int(rnd(1)*20)+1
429 print "voce rolou ataque: ";x
430 let y=int(rnd(1)*20)+1
431 print e$;" rolou defesa: "y
432 for j=1 to 200:next j
433 next i
434 if x>y then goto 437
435 if y>x then 440
436 if x=y then print "voce erra"

437 k=k+1
438 print "voce atacou ";e$:input o:y$="dano"
439 gosub 451:el=(el-rl):b=0:return

440 print e$;" defendeu seu ataque":input o:return


441 print chr$(147):rem placar
442 print "vitorias:":print "voce ";k,"inimigo: ";l
443 print "saude:":print "voce ";jl,"inimigo ";el
444 input o:return


445 rem jogador ganha
446 print "voce derrota ";e$:input o
447 let v=1:print chr$(147):return

448 rem jogador perde
449 print "voce foi derrotado por ";e$:input o
450 let v=0:print chr$(147):return


451 rem rolagem de d12
452 print chr$(147)
453 print "rolar d12 para ";y$:input rl
454 let x=0
455 rem por 7 vezes
456 for i=1 to 7
457 print chr$(147)
458 let x=int(rnd(1)*12)+1
459 print x
460 for j=1 to 200:next j
461 next i
462 print chr$(147)
463 print y$;":";x:input o
464 rl=x
465 return

466 rem fim da rolagem de batalha ===============================








470 ac(1)=1:e$="cadaver reanimado":el=18:print chr$(147):rem sala - poco de corpos 
471 print "uma especie de caverna umida e  putrida,iluminada apenas por um buraco"
472 print "no teto muito alto, no  meio  da pequenacaverna uma pilha gigantesca de"
473 print "cadaveres em diferentes estagios  de de-composicao."
474 print "aparentemente este local e um poco  ondecorpos de inimigos e prisioneiros"
475 print "sao jogados como lixo..."
476 print:print "1-vasculhar pilha de corpos"
477 input "2-dar meia volta";o
478 if o<>1 and o<>2 then goto 470:if o=1 then goto 480
479 if o=2 then goto 499
480 y$="percepcao":c=8:gosub 50:input o
481 if t>=c then goto 491
482 if t=1 then goto 483:if t<c then goto 487
483 print "enquanto afundava sua  mao  na  pilha decorpos para encontrar algo util"
484 print "voce sente uma mordida  muito  forte  noseu braco, voce se afasta em choque"
485 print "e da pilha  de corpos  um  ";e$;"  correpara te atacar...":jl=jl-2:f=0:input o:gosub 370
486 goto 494
487 print "enquanto revirava a pilha,  uma  mao  emdecomposicao te puxa com muita"
488 print "agrassividade,  ao tentar se  afastar um";e$;" agarrado no seu braco"
489 print "e puxado para fora da pilha de corpos, ese prepara para atacar...":f=0:input o:gosub 370
490 goto 494
491 print "enquanto revira  a  pilha  putrefata  decorpos voce nota um movimento"
492 print "estranho e rapidamente se afasta em pos-tura defensiva. da pilha"
493 print "de corpos sai um ";e$:f=1:input o:gosub 370
494 if v=2 then goto 499
495 if v=1 then goto 498
496 print chr$(147):print e$;" derrotou  voce,  suaalma esta aprisionada aqui"
497 print "para todo o sempre...":input o:goto 40
498 print "voce percebe que o ";e$;" derrotado der-ruba uma carta":input o:gosub 620
499 print "ao sair do poco de corpos, as  paredes eo teto colapsam."
500 print "nao sera mais possivel seguir este cami-nho...":input o:goto 150

520 rem masmorras nivel 2
521 print "ainda em desenvolvimento":input o:goto 150

550 rem biblioteca n2
551 print "ainda em desenvolvimento":input o:goto 290

580 rem cozinha do salao
581 print "ainda em desenvolvimento":input o:goto 350

620 rem carta de cadaver
621 print chr$(147):print "a carta diz:"
622 gosub 16:gosub 18
623 print "  ";chr$(116);"  a chave para  o nivel elevado do";chr$(121)
624 print "  ";chr$(116);"conhecimento, esta  sob o berco em";chr$(121)
625 print "  ";chr$(116);"que o mal repousa.                ";chr$(121)
626 gosub 18:gosub 16
627 print "apos ler a carta a mesma  se  transformaem uma fumaca escura, e"
628 print "se desfaz...":input o:return

650 rem explorar a biblioteca
651 print "ainda em desenvolvimento":input o:goto 290




998 goto 10
999 end

9000 dim ac(100):dim it(100)
9001 for i=1 to 100
9002 ac(i)=0:it(i)=0
9003 next i
9004 let f=0
9005 let e$="esqueleto"
9006 let jl=20
9007 let el=0
9008 let c=11
9009 let y$="fuga"
9010 let t=0
9040 return